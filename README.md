# rxnorm - an R package for working with the RxNorm drug naming system

<!-- badges: start -->
  [![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
  <!-- badges: end -->
  
[RxNorm](https://www.nlm.nih.gov/research/umls/rxnorm/index.html) provides normalized names for clinical drugs and links its names to many of the drug vocabularies commonly used in pharmacy management and drug interaction software, including those of First Databank, Micromedex, and Gold Standard Drug Database. By providing links between these vocabularies, RxNorm can mediate messages between systems not using the same software and vocabulary.

The RxNorm dataset is produced by the National Library of Medicine. 

Links to some documentation on RxNorm:

* [RxNorm Overview](https://www.nlm.nih.gov/research/umls/rxnorm/overview.html)
* [RxNorm Technical Documentation](https://www.nlm.nih.gov/research/umls/rxnorm/docs/index.html)
* [RxNorm REST API Documentation](http://mor.nlm.nih.gov/download/rxnav/RxNormAPIs.html)
* [RxNorm Browser](http://mor.nlm.nih.gov/download/rxnav/RxNavDoc.html)
* [RxNorm FAQ](https://www.nlm.nih.gov/research/umls/rxnorm/faq.html)

# Package details
This R package provides an interface to the RxNorm REST API's:

Fully implemented:

* [RxNorm API](https://rxnav.nlm.nih.gov/RxNormAPIs.html) for retrieving data from the [RxNorm](http://www.nlm.nih.gov/research/umls/rxnorm/) data set
* [RxClass API](https://rxnav.nlm.nih.gov/RxClassAPIs.html) for retrieving drug classes and drug members from a number of different drug class types.

Partially implemented:

Not yet implemented:

* [RxNorm Prescribable API](https://rxnav.nlm.nih.gov/PrescribableAPIs.html) for retrieving data from the [RxNorm Current Prescribable Content](http://www.nlm.nih.gov/research/umls/rxnorm/docs/prescribe.html)
* [RxTerms API](https://rxnav.nlm.nih.gov/RxTermsAPIs.html) for retrieving data for the [RxTerms](https://mor.nlm.nih.gov/RxTerms/) data set.
* [Drug Interaction API](https://rxnav.nlm.nih.gov/InteractionAPIs.html) for accessing drug-drug interactions
* [RxCUI History API](https://rxnav.nlm.nih.gov/RxcuiHistoryAPIs.html) for RxNorm concept history

## Installation
```r
remotes::install_gitlab("dkincaid/rxnorm")
```

## Usage
By default the package will use the base URI published by the RxNorm API documentation https://rxnav.nlm.nih.gov/REST/.
If you want to use this against a different installation of the API, use the `set_rx_norm_uri` function.

The names of the functions that implement the API endpoints are named using the "Functional Name"
in the [RxNorm API documentation](http://mor.nlm.nih.gov/download/rxnav/RxNormAPIs.html) except
that instead of camel case the functions use snake case (e.g. filterByProperty becomes filter_by_property).

## Reporting bugs/issues
For reporting any bugs, issues or questions please first review the already created 
list of issues at https://gitlab.com/dkincaid/rxnorm/-/issues. If you don't see
your problem addressed there please create a new issue.
