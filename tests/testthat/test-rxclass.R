test_that("find_classes_by_id works", {
  result <- find_classes_by_id("B01AA")
  expect_equal(result$rxclassMinConceptList$rxclassMinConcept[[1]]$classType, "ATC1-4")
})

test_that("find_class_by_name works", {
  result <- find_class_by_name("radiopharmaceuticals")
  expect_true(length(result$rxclassMinConceptList$rxclassMinConcept) > 0)
})

test_that("find_similar_classes_by_class works", {
  result <- find_similar_classes_by_class("768611005", rela_source = "SNOMEDCT", score_type = 1,
                                          rela = "isa_structure", top = 1)
  expect_true(length(result$similarityMember$ingredientRxcui) > 1)
})

test_that("find_similar_classes_by_drug_list works", {
  result <- find_similar_classes_by_drug_list("7052 7676 4804 23088", top = 3)
  expect_true(length(result$similarityMember$rankClassConcept) == 3)
})

test_that("get_all_classes works", {
  result <- get_all_classes()
  expect_true(length(result$rxclassMinConceptList$rxclassMinConcept) > 1)

  result <- get_all_classes(class_types = "MOA")
  expect_true(length(result$rxclassMinConceptList$rxclassMinConcept) > 1)
})

test_that("get_class_by_rxnorm_drugid works", {
  result <- get_class_by_rxnorm_drugid("7052", rela_source = "MEDRT", relas = "may_treat")
  expect_true(length(result$rxclassDrugInfoList$rxclassDrugInfo) > 1)
})

test_that("get_class_by_rxnorm_drug_name works", {
  result <- get_class_by_rxnorm_drug_name("morphine", rela_source = "MEDRT", relas = "may_treat")
  expect_true(length(result$rxclassDrugInfoList$rxclassDrugInfo) > 1)
})

test_that("get_class_contexts works", {
  result <- get_class_contexts("D019275")
  expect_true(length(result$classPathList$classPath) > 1)
})

test_that("get_class_graph_by_source works", {
  result <- get_class_graph_by_source("D065693", "MESHPA")
  expect_true(length(result$rxclassGraph$rxclassMinConceptItem) > 1)
})

test_that("get_class_members works", {
  result <- get_class_members("N0000008638", "DAILYMED", "has_PE")
  expect_equal(result$userInput$classId, "N0000008638")
})

test_that("get_class_tree works", {
  result <- get_class_tree("N0000185505", "MOA")
  expect_true(length(result) > 0)
})

test_that("get_class_types works", {
  result <- get_class_types()
  expect_true(length(result) > 10)
})

test_that("get_relas works", {
  result <- get_relas("dailymed")
  expect_true("has_moa" %in% result)
})

test_that("get_similarity_information works", {
  result <- get_similarity_information(class_id1 = "N02AA", rela_source1 = "ATC",
                                       class_id2 = "D009294", rela_source2 = "MESH")
  expect_true(length(result$SimilarityInformation$drugClassConcept) > 1)
})

test_that("get_sources_of_drug_class_relations works", {
  result <- get_sources_of_drug_class_relations()
  expect_true("ATC" %in% result)
})

test_that("get_class_spelling_suggestions works", {
  result <- get_class_spelling_suggestions("marcolides")
  expect_true("macrolides" %in% result)
})
