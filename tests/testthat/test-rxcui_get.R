test_that("get_all_properties works", {
  result <- get_all_properties("18600")
  expect_equal(result[[1]]$propValue, "IN")

  result <- get_all_properties("18600", "names codes")
  names <- unique(purrr::map_chr(result, "propCategory"))
  expect_equal(names, c("NAMES", "CODES"))
})

test_that("get_all_related_info works", {
  result <- get_all_related_info("866350")
  brand_name <- purrr::keep(result$allRelatedGroup$conceptGroup, function(x) x$tty == "BN" )
  #expect_equal()
})

test_that("get_all_historical_ndcs works", {
  result <- get_all_historical_ndcs("1668240")
  expect_true(length(result$historicalNdcConcept$historicalNdcTime) == 2)
})

test_that("filter_by_property works", {
  result <- filter_by_property("7052", "TTY")
  expect_equal(result$rxcui, "7052")

  result <- filter_by_property("7052", "TTY", "IN PIN")
  expect_equal(result$rxcui, "7052")

  result <- filter_by_property("7052", "TTYS")
  expect_null(result)
})

test_that("get_ndcs works", {
  result <- get_ndcs("213269")
  expect_true(length(result) > 5)
})

test_that("get_proprietary_information works", {
  conn <- umlsr::umlsConnect()
  ticket <- umlsr::getServiceTicket(conn)
  result <- get_proprietary_information("261455", ticket)
  expect_equal(result$proprietaryGroup$rxcui, "261455")
})

test_that("get_related_by_relationship works", {
  result <- get_related_by_relationship("174742", rela = "tradename_of has_precise_ingredient")
  expect_equal(result$relatedGroup$rxcui, "174742")
})

test_that("get_related_by_type works", {
  result <- get_related_by_type("174742", tty = "SBD SBDF")
  expect_equal(result$relatedGroup$rxcui, "174742")
})

test_that("get_rx_concept_properties", {
  result <- get_rx_concept_properties("131725")
  expect_equal(result$properties$rxcui, "131725")
  expect_equal(result$properties$name, "Ambien")
})

test_that("get_rxcui_history_status", {
  result <- get_rxcui_history_status("1791569")
  expect_equal(result$rxcuiStatusHistory$attributes$rxcui, "1791569")
})

test_that("get_rxnorm_name", {
  result <- get_rxnorm_name("161")
  expect_equal(result, "acetaminophen")
})

test_that("get_rx_property works", {
  result <- get_rx_property("7052", "ATC")
  expect_true(unique(purrr::map_chr(result, "propName")) == "ATC")
})


