% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/rxclass.R
\name{find_class_by_name}
\alias{find_class_by_name}
\title{Find drug classes from a class name}
\usage{
find_class_by_name(class_name, class_types = NULL, return_format = "json")
}
\arguments{
\item{class_name}{the name of the class}

\item{class_types}{(optional) a list of class types to check. See \link{get_class_types}
for a list of the possible class types. If none are specified, all class types
are checked for the name.}

\item{return_format}{the return format to use: "json" or "xml"}
}
\value{
list of class information for the specified identifier
}
\description{
Retrieve class information from a class name. The class name may be a synonym
for the class
}
\examples{
\dontrun{
  find_class_by_name("radiopharamceuticals")
}

}
\seealso{
\url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findClassByName}
}
