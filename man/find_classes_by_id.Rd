% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/rxclass.R
\name{find_classes_by_id}
\alias{find_classes_by_id}
\title{Find the drug classes from a class identifier}
\usage{
find_classes_by_id(class_id, return_format = "json")
}
\arguments{
\item{class_id}{the class identifier}

\item{return_format}{the return format to use: "json" or "xml"}
}
\value{
list of class information for the specified identifier
}
\description{
Retrieve class information from a class identifier.
}
\seealso{
\url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findClassById}
}
