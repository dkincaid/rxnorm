#' Find the drug classes from a class identifier
#'
#' Retrieve class information from a class identifier.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findClassById}
#'
#' @param class_id the class identifier
#' @template param_return_format
#'
#' @return list of class information for the specified identifier
#'
#' @exmaples \dontrun{
#'   find_classes_by_id("D058888")
#' }
#'
#' @export
find_classes_by_id <- function(class_id, return_format = "json") {
  result <- rx_class_api("/class/byId", params = list(classId = class_id),
                         return_format = return_format)
  return(result)
}

#' Find drug classes from a class name
#'
#' Retrieve class information from a class name. The class name may be a synonym
#' for the class
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findClassByName}
#'
#' @param class_name the name of the class
#' @param class_types  (optional) a list of class types to check. See [get_class_types]
#' for a list of the possible class types. If none are specified, all class types
#'  are checked for the name.
#' @template param_return_format
#'
#' @return list of class information for the specified identifier
#'
#' @examples \dontrun{
#'   find_class_by_name("radiopharamceuticals")
#' }
#'
#' @export
find_class_by_name <- function(class_name, class_types = NULL, return_format = "json") {
  result <- rx_class_api("class/byName", params = list(className = class_name,
                                                       classTypes = class_types),
                         return_format = return_format)
  return(result)
}

#' Find similar class membership
#'
#'
#' Find similar classes of drug members. The input drug class is defined by the
#' class identifier (classId), the source of drug relations (relaSource) and
#' the relationship (rela) of the drugs to the class. See
#' \href{similarity classes}{https://rxnav.nlm.nih.gov/RxClassSimilarity.html}
#' for more details on how the similar classes are determined.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findSimilarClassesByClass}
#'
#' @param class_id the class identifier
#' @param rela_source the source of the drug relations. This parameter defines
#'  the drugs in the input class (classId). See [get_class_members] for a list
#'  of the drug sources
#' @param rela the relationship of the drug class to its members. This parameter
#'  defines the drugs in the input class (classId). Should rela be empty,
#'  it can be omitted. See [get_class_members] for valid relationship values.
#' @param score_type (optional, default:0) the score type. 0 = Equivalence,
#'  1 = "includes" inclusion, 2 = "included_in" inclusion. See
#'  \href{similarity scoring}{https://rxnav.nlm.nih.gov/RxClassSimilarity.html#label:scoring}
#'  for details on how the scores are calculated. The difference between score
#'  type 1 (includes) and score type 2 (included_in) is as follows:
#'  * for score type 1 (includes), the input drug class includes the resulting drug classes.
#'  That is, the resulting drug classes have a large number of members which are included
#'  in the input drug class. This would be any resulting drug classes which have
#'  an inclusion score greater than 0.
#'  * for score type 2 (included_in), the input drug class is included in the
#'  resulting drug classes. That is, the input drug class has a large number of
#'  members contained in the returned drug classes. This would be any resulting
#'  drug classes which have an inclusion score less than 0.
#'  For score types 1 and 2, both the equivalence threshold and inclusion threshold
#'  are used to determine the resulting drug classes. For score type 0 only the
#'  equivalence threshold is used to determine the resulting drug classes.
#' @param top (optional, default:10) the number of results to return - note that
#'  more results than the number specified may be returned in the case of tie scores.
#' @param equivalence_threshold (optional, default:0.0) a threshold between 0.0
#'  and 1.0 for the equivalence values. All drug classes with equivalence values
#'  under the threshold will not be returned.
#' @param inclusion_threshold  (optional, default:1.0) a threshold between 0.0
#'  and 1.0 for inclusion scores. Only used for scoreType = 1 or 2. All drug
#'  classes with (the absolute value of) inclusion scores under the threshold
#'  will not be returned.
#' @template param_return_format
#'
#' @return list of the similarity classes
#'
#' @examples \dontrun{
#'     find_similar_classes_by_class("768611005", rela_source = "SNOMEDCT", score_type = 1,
#'                                   rela = "isa_structure", top = 1)
#' }
#'
#' @export
find_similar_classes_by_class <- function(class_id, rela_source, rela, score_type = NULL,
                                          top = NULL, equivalence_threshold = NULL,
                                          inclusion_threshold = NULL, return_format = "json") {
  result <- rx_class_api("class/similar",
                         params = list(classId = class_id,
                                       relaSource = rela_source,
                                       rela = rela,
                                       scoreType = score_type,
                                       top = top,
                                       equivalenceThreshold = equivalence_threshold,
                                       inclusionThreshold = inclusion_threshold),
                         return_format = return_format)
  return(result)
}

#' Find similar classes from a list of RxNorm drug identifiers
#'
#' Find similar classes for a list of drugs specified by RxNorm identifiers.
#' See \href{similarity classes}{https://rxnav.nlm.nih.gov/RxClassSimilarity.html}
#' for more details on how the similar classes are determined.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findSimilarClassesByDrugList}
#'
#' @param rxcuis a list of RxNorm drug identifiers (RxCUIs). A maximum of 500 are permitted.
#' @param rela_source (optional) the source of the drug relations. This parameter defines
#'  the drugs in the input class (classId). See [get_class_members] for a list
#'  of the drug sources
#' @param rela (optional) the relationship of the drug class to its members. This parameter
#'  defines the drugs in the input class (classId). Should rela be empty,
#'  it can be omitted. See [get_class_members] for valid relationship values.
#' @param score_type (optional, default:0) the score type. 0 = Equivalence,
#'  1 = "includes" inclusion, 2 = "included_in" inclusion. See
#'  \href{similarity scoring}{https://rxnav.nlm.nih.gov/RxClassSimilarity.html#label:scoring}
#'  for details on how the scores are calculated. The difference between score
#'  type 1 (includes) and score type 2 (included_in) is as follows:
#'  * for score type 1 (includes), the input drug class includes the resulting drug classes.
#'  That is, the resulting drug classes have a large number of members which are included
#'  in the input drug class. This would be any resulting drug classes which have
#'  an inclusion score greater than 0.
#'  * for score type 2 (included_in), the input drug class is included in the
#'  resulting drug classes. That is, the input drug class has a large number of
#'  members contained in the returned drug classes. This would be any resulting
#'  drug classes which have an inclusion score less than 0.
#'  For score types 1 and 2, both the equivalence threshold and inclusion threshold
#'  are used to determine the resulting drug classes. For score type 0 only the
#'  equivalence threshold is used to determine the resulting drug classes.
#' @param top (optional, default:10) the number of results to return - note that
#'  more results than the number specified may be returned in the case of tie scores.
#' @param equivalence_threshold (optional, default:0.0) a threshold between 0.0
#'  and 1.0 for the equivalence values. All drug classes with equivalence values
#'  under the threshold will not be returned.
#' @param inclusion_threshold  (optional, default:1.0) a threshold between 0.0
#'  and 1.0 for inclusion scores. Only used for scoreType = 1 or 2. All drug
#'  classes with (the absolute value of) inclusion scores under the threshold
#'  will not be returned.
#' @template param_return_format
#'
#' @return list of the similarity classes
#'
#' @examples \dontrun{
#'    find_similar_classes_by_drug_list("7052 7676 7804 23088", top = 3)
#' }
#'
#' @export
find_similar_classes_by_drug_list <- function(rxcuis, rela_source = NULL, rela = NULL, score_type = NULL,
                                              top = NULL, equivalence_threshold = NULL,
                                              inclusion_threshold = NULL, return_format = "json") {
  result <- rx_class_api("class/similarByRxcuis",
                         params = list(rxcuis = rxcuis,
                                       relaSource = rela_source,
                                       rela = rela,
                                       scoreType = score_type,
                                       top = top,
                                       equivalenceThreshold = equivalence_threshold,
                                       inclusionThreshold = inclusion_threshold),
                         return_format = return_format)
  return(result)

}

#' Get all drug classes
#'
#' Get all classes for each specified class type.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getAllClasses}
#'
#' @param class_types an array of class types. See [get_class_types] for the list
#'  of valid class types. If no class types are specified, then all classes of
#'  all types are returned.
#' @template param_return_format
#'
#' @return list of all classes
#'
#' @export
get_all_classes <- function(class_types = NULL, return_format = "json") {
  result <- rx_class_api("allClasses", params = list(classTypes = class_types),
                         return_format = return_format)
  return(result)
}

#' Get the classes of a specified drug identifier
#'
#' Get the classes that directly contain a product or ingredient related to a given RxNorm concept. The user can limit the classes returned by specifying a list of sources of drug-class relations, as well as a list of relations.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassByRxNormDrugId}
#'
#' @param rxcui the RxNorm identifier (RxCUI) of the drug. This can be an identifier for any RxNorm drug term.
#' @param rela_source (optional) the source of the drug-class relationships see
#' [get_sources_of_drug_class_relations]
#' for the list of sources of drug-class relations. If this field is omitted, all
#' sources of drug-class relationships will be used.
#' @param relas (optional) a list of relationships of the drug to the class.
#' This field is ignored if relaSource is not specified.
#' @template param_return_format
#'
#' @return a list of the classes for the RxCUI of the drug
#'
#' @examples \dontrun{
#'   get_class_by_rxnorm_drugid("7052", rela_source = "MEDRT", relas = "may_treat")
#' }
#'
#' @export
get_class_by_rxnorm_drugid <- function(rxcui, rela_source = NULL, relas = NULL,
                                       return_format = "json") {
  result <- rx_class_api("class/byRxcui", params = list(rxcui = rxcui,
                                                        relaSource = rela_source,
                                                        relas = relas),
                         return_format = "json")
  return(result)
}

#' Get the classes of a specified drug name
#'
#' Get the classes containing a specifed drug name as a member. The user may specify
#' a source of drug-class relations and relationship values to filter the output returned.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassByRxNormDrugName}
#'
#' @param drug_name the name of a generic or branded drug
#' @param rela_source (optional) the source of the drug-class relationships see
#'  [get_sources_of_drug_class_relations]
#' for the list of sources of drug-class relations. If this field is omitted, all
#' sources of drug-class relationships will be used.
#' @param relas (optional) a list of relationships of the drug to the class.
#' This field is ignored if relaSource is not specified.
#' @template param_return_format
#'
#' @return a list of the classes containing the drug name
#'
#' @examples \dontrun{
#'    get_class_by_rxnorm_drug_name("morphine", rela_source = "MEDRT", relas = "may_treat")
#' }
#'
#' @export
get_class_by_rxnorm_drug_name <- function(drug_name, rela_source = NULL, relas = NULL,
                                          return_format = "json") {
  result <- rx_class_api("class/byDrugName", params = list(drugName = drug_name,
                                                           relaSource = rela_source,
                                                           relas = relas),
                         return_format = return_format)
  return(result)
}

#' Get the class context
#'
#' Get the class contexts. A context is a path (a sequence of classes) starting
#' with the specified class and ending with the root of the class hierarchy.
#' Multiple paths are returned if the class hierarchy forms a directed acyclic
#' graph rather than a tree.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassContexts}
#'
#' @param class_id the class identifier
#' @template param_return_format
#'
#' @return a list containing the context for the class
#'
#' @export
get_class_contexts <- function(class_id, return_format = "json") {
  result <- rx_class_api("classContext", params = list(classId = class_id),
                         return_format = return_format)
  return(result)
}

#' Get the class graph of ancestors
#'
#' Get the graph of a specified class. This will return the ancestors of the
#' specified class in a graph form (nodes and edges).
#'
#' @param class_id the class identifier
#' @param source the class type. This is optional except for SNOMED CT identifiers
#'  (DISPOS or STRUCT class types). See [get_class_types] for a list of the
#'  possible class types.
#' @template param_return_format
#'
#' @return a list containing the graph of the specified class
#'
#' @export
get_class_graph_by_source <- function(class_id, source, return_format = "json") {
  result <- rx_class_api("classGraph", params = list(classId = class_id,
                                                     source = source),
                         return_format = return_format)
  return(result)
}

#' Get the drug members of a specified class
#'
#' Get the drug members of a specified class. This function requires the
#' specification of a source of drug-class relationships (for example: DailyMed)
#' as well as a relationship to the class.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassMembers}
#'
#' @param class_id the class identifier. Note that this is NOT an RxNorm identifier,
#' but an identifier from the source vocabulary.
#'
#' @param rela_source the source asserting the relationships between the drug members and the drug class.
#'
#' @param rela  the relationship of the drug class to its members
#'
#' @param trans 0 = include indirect and direct relations (the default). 1 = direct relations only.
#'
#' @param ttys include only drugs with the specified RxNorm term types. Default is IN, PIN and MIN.
#'
#' @template param_return_format
#'
#' @return list of the drug members of the specified class
#'
#' @examples \dontrun{
#'   get_class_members("N0000008638", "DAILYMED", "has_PE")
#'   get_class_members("N0000008638", "DAILYMED", "has_PE", ttys = "IN PIN")
#' }
#'
#' @export
get_class_members <- function(class_id, rela_source, rela, trans = NULL, ttys = NULL,
                              return_format = "json") {
  result <- rx_class_api("classMembers", params = list(classId = class_id,
                                                       relaSource = rela_source,
                                                       rela = rela,
                                                       trans = trans,
                                                       ttys = ttys),
                         return_format = return_format)
  return(result)
}


#' Get the descendents of a class
#'
#' Get the class tree for a class of the specified classId and optional classType.
#' The class tree represents that class and the classes that are its descendants.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassTree}
#'
#' @param class_id the class identifier
#' @param class_type (optional) the class type. See [get_class_types] for a list of the
#' possible class types. As some classIds are in multiple class types, it is
#' recommended that this field be specified, because this resource returns only
#' one class tree.
#' @template param_return_format
#'
#' @return the class tree as nested list
#'
#' @examples \dontrun{
#'   get_class_tree("N0000185505", "MOA")
#' }
#'
#' @export
get_class_tree <- function(class_id, class_type = NULL, return_format = "json") {
  result <- rx_class_api("classTree", params = list(classId = class_id,
                                                    classType = class_type),
                         return_format = return_format)
  return(result$rxclassTree)
}

#' Get the class types
#'
#' Get the class types.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassTypes}
#'
#' @return a list of the class types
#'
#' @export
get_class_types <- function() {
  result <- rx_class_api("classTypes", return_format = "json")
  return(result$classTypeList$classTypeName)
}

#' Get the relationships for a source of drug relations
#'
#' Get the relationships for a source of drug relations. See the
#' \href{RxClass Overview}{https://rxnav.nlm.nih.gov/RxClassIntro.html}
#' for a table that shows the relationships each drug source uses to identify
#' with the drug class.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getRelas}
#'
#' @param rela_source  the source of the drug relations. See [get_sources_of_drug_class_relations]
#' for the list of sources of drug-class relations.
#'
#' @return a list of the relationships for the source
#'
#' @export
get_relas <- function(rela_source) {
  result <- rx_class_api("relas", params = list(relaSource = rela_source), return_format = "json")
  return(result$relaList$rela)
}

#' Get the similarity information between members of two classes
#'
#' Return the similarity information between two drug classes. See
#' \href{similarity classes}{https://rxnav.nlm.nih.gov/RxClassSimilarity.html}
#' for more details on how the similar classes are determined.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getSimilarityInformation}
#'
#' @param class_id1 the class identifier of the first class
#' @param rela_source1 the source of the drug relations for the first class. See
#' [get_class_members] for a list of the drug sources
#' @param rela1 the relationship of the drug class to its members for the first class.
#' See [get_class_members] for valid relationship values.
#' @param class_id2 the class identifier of the second class
#' @param rela_source2 the source of the drug relations for the second class
#' @param rela2 the relationship of the drug class to its members for the second class
#' @template param_return_format
#'
#' @return list of the similarity information between the two drug classes
#'
#' @examples \dontrun{
#'    get_similarity_information(class_id1 = "N02AA", rela_source1 = "ATC",
#'                               class_id2 = "D009294", rela_source2 = "MESH")
#' }
#' @export
get_similarity_information <- function(class_id1, rela_source1, rela1 = NULL,
                                       class_id2, rela_source2, rela2 = NULL,
                                       return_format = "json") {
  result <- rx_class_api("class/similarInfo", params = list(classId1 = class_id1,
                                                            relaSource1 = rela_source1,
                                                            rela1 = rela1,
                                                            classId2 = class_id2,
                                                            relaSource2 = rela_source2,
                                                            rela2 = rela2),
                         return_format = return_format)
  return(result)
}

#' Get the sources of drug-class relations
#'
#' Get the list of sources that associate generic drugs to the class types.
#' Resources [get_class_by_rxnorm_drug_name] and [get_class_members] use
#' drug sources as function inputs.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getSourcesOfDrugClassRelations}
#'
#' @return a list of the relations source names
#'
#' @export
get_sources_of_drug_class_relations <- function() {
  result <- rx_class_api("relaSources", return_format = "json")
  return(result$relaSourceList$relaSourceName)
}

#' Get the spelling suggestions for a drug or class name.
#'
#' Get the spelling suggestions for a drug or class name.
#'
#' @seealso \url{https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getSpellingSuggestions}
#'
#' @param term the name of the drug or class
#' @param type (optional, default: CLASS) type of name. Valid values are "DRUG"
#' or "CLASS" for a drug name or class name, respectively
#'
#' @return list of spelling suggestions
#'
#' @export
get_class_spelling_suggestions <- function(term, type = NULL) {
  result <- rx_class_api("spellingsuggestions", params = list(term = term,
                                                              type = type),
                         return_format = "json")
  return(result$suggestionList$suggestion)
}

